```
       ██   ███████   ██      
     ██░   ██░░░░░██ ░░ ██    
   ██░    ██     ░░██  ░░ ██  
 ██░     ░██      ░██    ░░ ██
░░ ██    ░██      ░██     ██░ 
  ░░ ██  ░░██     ██    ██░   
    ░░ ██ ░░███████   ██░     
      ░░   ░░░░░░░   ░░       
```

> "Du har vackra ögon."

-- _Zweedse uitspraak om gegarandeerd chickies te scoren_

Info
----

- @0830 begint het SmP-gezeik
- melden: 0800--0830 => badge ophalen => met badge kan je kleding halen
- kleding: (KIA)KUA bij textielservice (A66)
- we mogen in pauzes en in (vrije) tussentijd niet eens op de afdeling blijven; we moeten dan naar de `Binnentuin (bij het restaurant` of naar de `Medische bibliotheek (locatie A05)`

- er is een **reflectiemoment @1200(--1300) op locatie A03.05**. **We moeten samen een thema kiezen om aan te werken tijdens het 'reflectiemoment'**
- Themata:
    + **01/ Communicatie tussen patiënt en specialist**  
      In deze opdracht kijken we hoe de communicatie was tussen de patient en de specialist. Welke positieve punten en welke ontwikkelpunten heb je gezien? Wat past bij jou als toekomstig specialist?
    + **02/ Efficiënt werken versus patiëntcontact**  
      In deze opdracht kijk je naar hoe de specialist zijn tijd en aandacht verdeeld tussen de patiënt en efficiënt werken. Wat vind jij hierin belangrijk als toekomstig specialist?
    + **03/ Jij tegenover de specialist**  
      In deze opdracht kijken we hoe de communicatie was tussen de patient en de specialist. Daarnaast kijken we hoe de communicatie was tussen jou en de patient. Vervolgens leggen we dit naast elkaar om te kijken waar je staat in je professionele ontwikkeling.
    + **04/ Medisch handelen van de specialist**  
      In deze opdracht kijk je naar wat je de specialist hebt zien doen op het gebied van medisch handelen en wat je daarvan vindt. Wat zou jij ook zo doen? Maar ook misschien wat je anders zou doen? Wat zegt dit over jouw beeld als toekomstig specialist?
 
> Je zult dus maar **twee van bovenstaande thema’s (?? huh ??)** behandelen tijdens SMP in CWZ. Daarnaast bieden we nog een opdracht aan die je kan ondersteunen bij het ontdekken waar je staat in je professionele identiteit. Deze opdracht kun je alleen maken maar ook met medestudenten bespreken. We adviseren je om deze opdracht te maken nadat je beide\[(n)] contactmomentern hebt gehad

Contact
-------

- name: Suzan Koopman
- func: Secretaresse stagecoördinatiepunt | Unit het Leerhuis J1.43
- org: Canisius-Wilhelmina Ziekenhuis
- loc: Kantorencomplex Jonkerbosch | Burg Daleslaan 23 | Nijmegen
- post: Postbus 9015 | 6500 GS Nijmegen
- tel `024 365 5148`
- mail: [s.koopman@cwz.nl](mailto:s.koopman@cwz.nl)

Ophthalmology
=============

<!--
**Pandoc Pipe tables**

Pandoc Pipe tables is the same as Multi Markdown, you have to switch into 
Multi Markdown if you use this table style.

**Multi Markdown/Pandoc Pipe tables**

Alignment:

|    Name   | Phone | Age Column |
| :-------- | :---: | ---------: |
| Anna      |   12  |         20 |
| Alexander |   13  |         27 |


| Right | Left | Default | Center |
| ----: | :--- | ------- | :----: |
|    12 | 12   |      12 |   12   |
|   123 | 123  |     123 |  123   |
|     1 | 1    |       1 |   1    |


Colspan(alpha status):

|              |           Grouping          ||
| First Header | Second Header | Third Header |
| ------------ | :-----------: | -----------: |
| Content      |         *Long Cell*         ||
| Content      |    **Cell**   |         Cell |
| New section  |      More     |         Data |
| And more     |    And more   |              |
| :---------------------------------------: |||
-->

Glossary
--------

|   abbr   |       def       |
| :------- | :-------------- |
| parasymp | parasympathetic |
| symp     | sympathetic     |
| dpt      | dioptre         |

|       term      |                                                                                                                                                                                def                                                                                                                                                                                 |
| :-------------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| refraction[^1]  | In physics, refraction is the change in direction of a wave passing from one medium to another or from a gradual change in the medium.                                                                                                                                                                                                                             |
| diffraction[^2] | Diffraction refers to various phenomena that occur when a wave encounters an obstacle or a slit. It is defined as the bending of waves around the corners of an obstacle or through an aperture into the region of geometrical shadow of the obstacle/aperture. The diffracting object or aperture effectively becomes a secondary source of the propagating wave. |

NB: note the 'subtle' difference in terminology of 'refraction' vs 'diffraction'. 

**accomodation**: `contraction M. ciliaris => relaxation Zonula ciliaris => lens becomes more convex => greater refraction`  
**emmetropia**: `good sight without the need for optical correction (glasses, contact lenses)`  


[^1]: https://en.wikipedia.org/wiki/Refraction
[^2]: https://en.wikipedia.org/wiki/Diffraction

Anatomy
-------

![anatomy](https://1re4xlezju-flywheel.netdna-ssl.com/wp-content/uploads/2016/08/human-eye-anatomy.jpeg)

Physiology
----------

Cornea, lens (_Lens crystallin_) and Corpus vitreum refract incoming light; total refraction is 58--65 dpt.

|   struc    |            func           |                contents                |
| :--------- | :------------------------ | :------------------------------------- |
| Cornea     | - refraction ~40--45 dpt  | many sensory neuron endings            |
| Lens       | - refraction ~16--22 dpt  | 35% crystallines (refracting proteins) |
| C. vitreum | - mechanical sturdiness   | eye compartment with largest volume:   |
|            | - translucent (for light) | - 98% H2O                              |
|            |                           | - 2% hyaluronic acid (_HA_) + collagen        |

| **Uvea** / components | func                                         |
| :---------        | :------------------------------------------- |
| Choroidea         | blood/nutrient supply to retina              |
| C. ciliare        | - accomodation (parasymp => M. ciliaris)     |
|                   | - production vitreous fluid                  |
| Iris              | incoming light regulation                    |
|                   | - parasymp => M. sphinter pupillae => myosis |
|                   | - symp => M. dilatator pupillae => mydriasis |

|      |      cones      |            rods           |
| :--- | :-------------- | :------------------------ |
| amnt | 6M              | 120M                      |
| loc  | macula + fovea  | periphery                 |
| func | - zien in licht | - zien in schermer/donker |
|      | - kleurenzien   |                           |
|      | - scherpzien    |                           |



Een normaal (emmetroop) oog breek het licht in alle richtingen even sterk. Bij refractieafw ligt het brandpunt niet _op_ de retina, maar  
- ervoor (_myopia_/bijziend) of  
- erachter (_hypermetropia_/verziend)  
- meerdere brandpunten doordat het oog het lichtniet in alle richtingen even sterk breekt (_astigmatisme_/cilindrische afwijking)  

![refrafw](https://www.best-home-remedies.com/wp-content/uploads/2018/04/Refractive-Error.jpg)

## KR ##

- Dubbelzien
    + monoculair
        * refractieafwijking
        * cataract
    + binoculair
        * manifest strabismus
        * myogeen (myasthenia Gravis, M. Graves, MS)
        * Neurogeen (uitval CN. III, IV en/of VI)

- Flitsen en gezichtsvelduitval
    + Flitsen
        * achterste glasvochtmembraanloslating
        * oculaire migraine
    + Flitsen + gezichtsuitval
        * ablatio retinae
    + Gezichtsuitval 
        * glaucoom
        * Opticus neuropathie
        * retinale vaatocclusie
        * hersentumor

- Rood oog
    + Pijnlijk
        * Uveitis anterior
        * keratitis
        * cornea-erosie
        * corneaal corpus alienum
        * scleritis
        * acuut glaucoom
    + Pijnloos
        * conjunctivitis (allergisch/viraal/bacterieel)
        * episcleritis
        * subconjunctivale bloeding
        * blepharitis

- Pijn
    + Pijn
        * neuritis optica
        * actieve Graves orbitopathie
        * cellulitis orbitae
    + Pijn rondom het oog
        * cellulitis orbitae
        * actieve Graves orbitopathie
        * hordeolum
        * arteriitis temporalis
        * migraine/hoofdpijn
        * scleritis
    + Irritatie
        * blepharitis
        * droge ogen
        * conjunctivitis
        * ectropion/entropion
        * trichiasis
    + Erge pijn
        * uveitis anterior
        * keratitis
        * cornea-erosie
        * cornea ulcus
        * acuut glaucoom
        * scleritis

- Visusdaling
    + Geleidelijk
        * refractie afw.
        * cataract
        * keratoconus
        * droge MD (maculadegeneratie)
        * DRP (diabetic retinopathy)
        * droge ogen
    + Acuut
        * Pijnlijk
            - neuritis optica
            - acuut glaucoom
            - cellulitis orbitae
            - arteriitis temporalis
            - trauma
        * Pijnloos
            - AION  (_anterior ischaemic optic neuropathy_)
            - retinale vaatocclusie
            - ablatio retinae
            - glasvochtbloeding
            - natte MD

> [wiki -- AION](https://en.wikipedia.org/wiki/Anterior_ischemic_optic_neuropathy): "_Anterior ischemic optic neuropathy (AION) is a medical condition involving loss of vision caused by damage to the optic nerve as a result of insufficient blood supply (ischemia). This form of ischemic optic neuropathy is generally categorized as two types: arteritic AION (or AAION), in which the loss of vision is the result of an inflammatory disease of arteries in the head called temporal arteritis, and non-arteritic AION (abbreviated as NAION, or sometimes simply as AION), which is due to non-inflammatory disease of small blood vessels._"

Pathology
---------

### Macular degeneration (MD) ###

- MD is een degeneratieve aandoening van de gele vlek (_macula lutea_).
    + droge vorm: langzaam progressief, kan overgaan in natte vorm
    + natte vorm: plotselinge, ernstige visusdaling
- Epix:
    + prvl: 37% vd ouderen >75yo in NL
        * 80--90% droge MD
        * 10--15% natte MD
- Sx:
    + visusdaling, 
    + metamorphopsia: 
        * ![metamorphopsia](https://www.cureamd.org/wp-content/uploads/2017/08/Amsler-Grid-in-AMD.gif) 
    + central scotoma:
        * ![centraal scotoom](http://iahealth.net/wp-content/uploads/2013/03/central_scotoma.jpg)
- RF:
    + hoge leeftijd
    + vrouwelijk geslacht
    + positieve fmHx
    + roken
    + voeding (te weinig groente en fruit (hypovitaminosis??))
- Dx:
    + visusmeting
    + amsler grid
    + fundoscopy
    + [OCT (optical coherence tomography)](https://www.aao.org/eye-health/treatments/what-is-optical-coherence-tomography)
    + [FAG (fluorescent angiography)](https://en.wikipedia.org/wiki/Fluorescein_angiography)
- Tx:
    + Leefstijlveranderingen:
        * roken staklen
        * gezonde voeding
        * optische hulpmiddelen
    + natte MD:
        * intravitreale anti-VEGF-injecties; vaak langdurig
        * (pharmacoTx mogelijk)
    + droge MD:
        * (geen pharmacoTx)
 
### Glaucoom ###

- progressieve opticusneuropathie met excavatie van de papil:
    + ![papil](https://upload.wikimedia.org/wikipedia/commons/4/4f/Glaukompapille2.jpg)
    + ![cupdisk](https://image.slidesharecdn.com/87c572ea-d803-447e-bc86-4ff7ca9499a6-150920204858-lva1-app6891/95/glaucoma-21-638.jpg?cb=1442782209)
- Epix:
    + prvl: 2nd meest voorkomende oorzaak van blindheid wereldwijd
- Sx:
    + symptoomloos
    + visusdaling en/of gezichtsveldverlies; pas in een laat stadium
- Aex:
    + beschadiging N. opticus
    + disbalans aanmaak/afvoer van kamervocht
        * verstopping trabeculae met belemmering van afvoer
        * oculaire/systemische afwijking
    + kamerwater:
       * `corpus ciliare => achterste oogkamer => pupil => voorste oogkamer => trabeculae => sinus venosus sclerae (Schlemm's canal)` 
       * ![schlemm](https://en.wikipedia.org/wiki/Schlemm%27s_canal#/media/File:Blausen_0390_EyeAnatomy_Sectional.png)
+ Dx:
    * gezichtsveldonderzoek
    * gonioscopie
    * tonometrie
    * fundoscopie papil
    * OCT
+ Tx:
    * phaphamacaTx: remming aanmaak, verbetering afvoer vvan kamerwater
    * laser
    * chirurgie
+ Px:
    * chronic, progressive;
    * slechtziendheid met eindstadium blindheid
+ DDx:
    * open-angle glaucoma
        - most prevalent
        - gezichtsvelduitval; lang symptoomloos
        - door verstopping trabeculae met belemmering van kamerwater afvoer
        - oculaire/systemische afw
    * closed-angle glaucoma
        - EMERGENCY
        - nauwe kamerhoek
        - ![kamerhoek](https://www.mayoclinic.org/-/media/kcms/gbs/patient-consumer/images/2013/08/26/10/41/ds00283_im00143_r7_openanglethu_jpg.jpg")
        - ![angle-closure](https://healthjade.com/wp-content/uploads/2019/04/angle-closure-glaucoma.jpg)
